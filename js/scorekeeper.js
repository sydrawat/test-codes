// console.log('connected');
var p1_disp = document.querySelector('#p1_disp');
var p2_disp = document.querySelector('#p2_disp');


var p1 = document.querySelector('#p1');
var p2 = document.querySelector('#p2');

var reset = document.querySelector('#reset');

var ip = document.querySelector('input');
var p = document.querySelector('#limit');

var p1_score = 0;
var p2_score = 0;
var winScore = 5;

var gameOver = false;


p1.addEventListener('click', function() {
    if (!gameOver){
        p1_score += 1;
        if (p1_score === winScore) {
            p1_disp.classList.add('winner');
            gameOver = true;
        }
        p1_disp.textContent = p1_score;
    }
});

p2.addEventListener('click', function() {
    if (!gameOver) {
        p2_score += 1;
        if (p2_score === winScore) {
            p2_disp.classList.add('winner');
            gameOver = true;
        }
        p2_disp.textContent = p2_score;
    }
});

reset.addEventListener('click', function() {
    resetgame();
});

function resetgame() {
    p1_disp.textContent = '0';
    p2_disp.textContent = '0';
    p1_score = 0;
    p2_score = 0;
    p1_disp.classList.remove('winner');
    p2_disp.classList.remove('winner');
    gameOver = false;
}


ip.addEventListener('change', function() {
    p.textContent = this.value;
    winScore = Number(this.value);
    resetgame();
});