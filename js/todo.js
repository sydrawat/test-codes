

window.setTimeout(function() {
    // put all of your JS code from the lecture here
    var todo = [];
    var ip = prompt("what do you want to do?");
    while (ip !== 'quit') {
      if (ip === 'list') {
        // console.log(todo);
        //call list function()
        listTodo();
      }
      else if (ip === 'new') {
        //call add function()
        addTodo();
      }
      else if (ip === 'delete') {
        //call delete function()
        deleteTodo();        
      }
      var ip = prompt("what do you want to do?");
    }
  
  //LIST FUNCTION
  function listTodo() {
    console.log('**********');
    todo.forEach(function(item, i) {
      console.log('['+i+'] : '+item);
      
    });
    console.log('**********');
  }
  //ADD FUNCTION
  function addTodo() {
    var newTodo = prompt('enter a new todo item');
    todo.push(newTodo);
    console.log('Added todo');
  }
  //DELETE FUNCTION
  function deleteTodo() {
    var idx = prompt('Enter index of todo to delete');
    todo.splice(idx,1);// go to index 'idx' and delete '1' number of array items.
    console.log('Deleted todo');
  }


  }, 500);