// $("#btn").on('click', function(){
//     $.ajax({
//         method: "GET",
//         url: "https://baconipsum.com/api/?type=meat-and-filler",
//         dataType: 'json'
//     })
//     .done(function(data){
//         console.log(data);
//         $(".para1").text(data[0]);
//     })
//     .fail(function(){
//         console.log("FAIL!");
//     });
// });


//          SHORT-HANDS FOR AJAX

$("#btn1").on('click', function(){
    $.get('https://baconipsum.com/api/?type=meat-and-filler')
    .done(function(data){
        console.log(data);
    })
    .fail(function(){
        console.log("ERROR!");
        
    });
});

$("#btn2").on('click', function(){
    var dataArray = {name: "Sid", age: "23"};
    $.post('https://baconipsum.com/api/?type=meat-and-filler')
    .done(function(data){
        console.log(data);
    })
    .fail(function(){
        console.log("ERROR!");
        
    });
});

$("#btn3").on('click', function(){
    $.getJSON('https://baconipsum.com/api/?type=meat-and-filler')
    .done(function(data){
        console.log(data);
    })
    .fail(function(){
        console.log("ERROR!");
        
    })
});