var age = prompt("Enter age : ");

var days = age * 365.25;

alert("You have been alive for roughly "+ days + " days");

// isEven()
function isEven(num) {
    if (num % 2 === 0) {
        console.log("number is even");
    }
    else {
        console.log("number is odd");
    }
}

// factorial
function factorial (num) {
    if (num === 1) {
        return 1;
    }
    else {
        return num * factorial(num - 1);
    }
}

// kebab-case to snake_case
function snake_case(name) {
    var res = name.replace(/-/g, "_"); // using regex, match anything inside / / with the g flag
    return res;
}