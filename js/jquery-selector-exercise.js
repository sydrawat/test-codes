if(jQuery){
    console.log('jQuery loaded!');
} else {
    alert('No jQuery!');
}

$("div").css("background", "purple");
$(".highlight").css({
    width: 200
});
$("#third").css("border", "3px solid orange");

// bonus
$("div:first-of-type").css('color', 'pink');// first of type is a CSS property
$("div:first").css('color', 'pink');// css shortcut, not native to JS
// document.querySelector('div').style.color='pink';