// console.log('connected');

//printReverse()
function printReverse(arr) {
    console.log('printReverse()');
    for (var i = arr.length - 1 ; i >= 0 ; i--) {
        console.log(arr[i]);
    }
}

//isUniform()
function isUniform(arr) {
    console.log('isUniform()');
    const first = arr[0];
    for (var i = 1; i < arr.length; i++) {
        if (first === arr[i]) {
            continue;
        }
        else {
            return false;
        }
    }
    return true;
}

//sumArray()
function sumArray(arr) {
    console.log('sumArray()');
    var sum = 0;
    for (let i = 0; i < arr.length; i++) {
        sum += arr[i];
    }
    return sum;
}

//max()
function max(arr) {
    console.log('max()');
    var max = 0;
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] >= max) {
            max = arr[i];
        }
    }
    return max;
}


printReverse(['a','b','c']);
console.log(isUniform(['a','a','a']));
console.log(isUniform(['a','b','c']));
console.log('sum = '+sumArray([1,2,3,4]));
console.log('max = '+max([1,22,3,4]));
